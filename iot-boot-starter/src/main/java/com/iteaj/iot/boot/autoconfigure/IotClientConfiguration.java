package com.iteaj.iot.boot.autoconfigure;

import com.iteaj.iot.IotThreadManager;
import com.iteaj.iot.boot.condition.ConditionalOnIotClient;
import com.iteaj.iot.boot.core.IotCoreConfiguration;
import com.iteaj.iot.client.http.HttpManager;
import com.iteaj.iot.client.http.okhttp.OkHttpManager;
import com.iteaj.iot.client.websocket.WebSocketClientListener;
import com.iteaj.iot.client.websocket.impl.DefaultWebSocketClientComponent;
import com.iteaj.iot.client.websocket.impl.DefaultWebSocketClientProtocolHandle;
import com.iteaj.iot.client.websocket.impl.DefaultWebSocketListenerManager;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;

import java.util.List;

@Order(88168)
@ConditionalOnIotClient
@AutoConfigureAfter(IotCoreConfiguration.class)
@EnableConfigurationProperties(IotClientProperties.class)
public class IotClientConfiguration {

    private IotClientProperties properties;
    protected final IotThreadManager threadManager;

    public IotClientConfiguration(IotThreadManager threadManager, IotClientProperties properties) {
        this.properties = properties;
        this.threadManager = threadManager;
    }

//    @Bean("httpManager")
//    @ConditionalOnMissingBean(HttpManager.class)
    public HttpManager httpManager() {
        return new OkHttpManager();
    }

    @Bean
    @ConditionalOnBean(WebSocketClientListener.class)
    public DefaultWebSocketClientComponent defaultWebSocketClientComponent(DefaultWebSocketListenerManager listenerManager) {
        return new DefaultWebSocketClientComponent(listenerManager);
    }

    @Bean
    @ConditionalOnBean(WebSocketClientListener.class)
    public DefaultWebSocketClientProtocolHandle defaultWebSocketClientProtocolHandle(DefaultWebSocketClientComponent clientComponent) {
        return new DefaultWebSocketClientProtocolHandle(clientComponent);
    }

    @Bean
    @ConditionalOnBean(WebSocketClientListener.class)
    public DefaultWebSocketListenerManager defaultWebSocketListenerManager(List<WebSocketClientListener> listeners) {
        return new DefaultWebSocketListenerManager(listeners);
    }
}
