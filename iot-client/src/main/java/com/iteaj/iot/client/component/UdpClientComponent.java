package com.iteaj.iot.client.component;

import com.iteaj.iot.client.ClientConnectProperties;
import com.iteaj.iot.client.MultiClientManager;
import com.iteaj.iot.client.SocketClient;
import com.iteaj.iot.client.UdpSocketClient;
import com.iteaj.iot.client.udp.UdpClientConnectProperties;
import com.iteaj.iot.client.udp.UdpClientMessage;
import io.netty.channel.socket.DatagramPacket;

/**
 * 基于udp实现的客户端
 */
public abstract class UdpClientComponent<M extends UdpClientMessage> extends SocketClientComponent<M, DatagramPacket> {

    public UdpClientComponent() { }

    public UdpClientComponent(UdpClientConnectProperties config) {
        super(config);
    }

    public UdpClientComponent(UdpClientConnectProperties config, MultiClientManager clientManager) {
        super(config, clientManager);
    }

    @Override
    public UdpSocketClient createNewClient(ClientConnectProperties config) {
        if(!(config instanceof UdpClientConnectProperties)) {
            throw new IllegalArgumentException("Udp协议的客户端组件只支持配置类型[UdpClientConnectProperties]");
        }

        return new UdpSocketClient(this, (UdpClientConnectProperties) config);
    }

    @Override
    public SocketClient createNewClientAndConnect(ClientConnectProperties config) {
        return super.createNewClientAndConnect(config);
    }
}
