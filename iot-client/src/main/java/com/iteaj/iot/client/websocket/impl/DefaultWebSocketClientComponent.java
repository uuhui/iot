package com.iteaj.iot.client.websocket.impl;

import com.iteaj.iot.AbstractProtocol;
import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.SocketMessage;
import com.iteaj.iot.client.ClientConnectProperties;
import com.iteaj.iot.client.MultiClientManager;
import com.iteaj.iot.client.SocketClient;
import com.iteaj.iot.client.TcpSocketClient;
import com.iteaj.iot.client.codec.WebSocketClient;
import com.iteaj.iot.client.protocol.ServerInitiativeProtocol;
import com.iteaj.iot.client.websocket.WebSocketClientComponentAbstract;
import com.iteaj.iot.client.websocket.WebSocketClientConnectProperties;
import com.iteaj.iot.client.websocket.WebSocketClientListener;
import com.iteaj.iot.websocket.WebSocketException;

import java.net.URI;
import java.util.List;

public class DefaultWebSocketClientComponent extends WebSocketClientComponentAbstract<DefaultWebSocketClientMessage> {

    private DefaultWebSocketListenerManager listenerManager;

    public DefaultWebSocketClientComponent(DefaultWebSocketListenerManager listenerManager) {
        this.listenerManager = listenerManager;
    }

    public DefaultWebSocketClientComponent(DefaultWebSocketListenerManager listenerManager, WebSocketClientConnectProperties config) {
        super(config);
        this.listenerManager = listenerManager;
    }

    public DefaultWebSocketClientComponent(MultiClientManager clientManager, DefaultWebSocketListenerManager listenerManager) {
        super(null, clientManager);
        this.listenerManager = listenerManager;
    }

    @Override
    public TcpSocketClient createNewClient(ClientConnectProperties config) {
        return new WebSocketClient(this, (WebSocketClientConnectProperties) config);
    }

    @Override
    public String getName() {
        return "websocket(Client)";
    }

    @Override
    public String getDesc() {
        return "WebSocket客户端默认实现";
    }

    @Override
    public WebSocketClientConnectProperties getConfig() {
        return (WebSocketClientConnectProperties) super.getConfig();
    }

    @Override
    public SocketMessage createMessage(byte[] message) {
        return new DefaultWebSocketClientMessage(message);
    }

    @Override
    public AbstractProtocol getProtocol(DefaultWebSocketClientMessage message) {
        return new DefaultWebSocketClientProtocol(message);
    }

    @Override
    protected ServerInitiativeProtocol doGetProtocol(DefaultWebSocketClientMessage message, ProtocolType type) {
        return null;
    }

    @Override
    public void start(Object config) {
        super.start(config);
        this.listenerManager.getListeners().forEach(item -> {
            this.createNewClientAndConnect(item.properties());
        });
    }

    @Override
    public SocketClient createNewClientAndConnect(ClientConnectProperties config) {
        if(config instanceof WebSocketClientConnectProperties) {
            if(((WebSocketClientConnectProperties) config).getListener() == null) {
                URI uri = ((WebSocketClientConnectProperties) config).getUri();
                // 查找可以处理此uri的监听器
                WebSocketClientListener listener = listenerManager.getListenerByURI(uri);
                if(listener != null) {
                    ((WebSocketClientConnectProperties) config).bindListener(listener);
                } else {
                    logger.error("uri["+uri.getAuthority()+uri.getPath()+"]没有匹配到监听器");
                }
            }
        } else {
            throw new WebSocketException("websocket协议请使用配置类型["+WebSocketClientConnectProperties.class.getSimpleName()+"]");
        }
        return super.createNewClientAndConnect(config);
    }

}
