package com.iteaj.iot.event;

import com.iteaj.iot.FrameworkComponent;

import java.util.EventObject;

public class IotEvent extends EventObject {

    private FrameworkComponent component;

    /**
     * Create a new {@code ApplicationEvent}.
     *
     * @param source the object on which the event initially occurred or with
     *               which the event is associated (never {@code null})
     */
    public IotEvent(Object source) {
        super(source);
    }

    public FrameworkComponent getComponent() {
        return component;
    }

    public IotEvent setComponent(FrameworkComponent component) {
        this.component = component;
        return this;
    }
}
