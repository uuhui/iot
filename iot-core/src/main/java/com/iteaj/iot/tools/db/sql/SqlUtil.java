package com.iteaj.iot.tools.db.sql;

public class SqlUtil {

    /**
     * 转换为{@link java.sql.Timestamp}
     *
     * @param date {@link java.util.Date}
     * @return {@link java.sql.Timestamp}
     * @since 3.1.2
     */
    public static java.sql.Timestamp toSqlTimestamp(java.util.Date date) {
        return new java.sql.Timestamp(date.getTime());
    }
}
