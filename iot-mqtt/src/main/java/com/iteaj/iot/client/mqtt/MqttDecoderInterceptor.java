package com.iteaj.iot.client.mqtt;

import com.iteaj.iot.codec.filter.DecoderInterceptor;
import io.netty.handler.codec.mqtt.MqttMessage;
import io.netty.handler.timeout.IdleState;

public interface MqttDecoderInterceptor<C extends MqttClientComponent> extends DecoderInterceptor<C> {

    MqttDecoderInterceptor DEFAULT = new MqttDecoderInterceptor(){};

    /**
     * Mqtt协议在超时后做ping请求
     * @param deviceSn
     * @param state
     * @return
     */
    @Override
    default Object idle(String deviceSn, IdleState state) {
        return MqttMessage.PINGREQ;
    }
}
