package com.iteaj.iot.plc.siemens;

import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.client.ClientConnectProperties;
import com.iteaj.iot.client.TcpSocketClient;
import com.iteaj.iot.client.component.TcpClientComponent;
import com.iteaj.iot.client.protocol.ServerInitiativeProtocol;
import com.iteaj.iot.plc.PlcException;

/**
 * 西门子S7编解码组件
 */
public class SiemensS7Component extends TcpClientComponent<SiemensS7Message> {

    public SiemensS7Component() { }

    public SiemensS7Component(SiemensConnectProperties config) {
        super(config);
    }

    @Override
    public TcpSocketClient createNewClient(ClientConnectProperties config) {
        if(config instanceof SiemensConnectProperties) {
            return new SiemensS7Client(this, (SiemensConnectProperties) config);
        } else {
            throw new PlcException("不支持的西门子PLC连接配置 - 请使用[SiemensConnectProperties]");
        }
    }

    @Override
    public Class<SiemensS7Message> getMessageClass() {
        return SiemensS7Message.class;
    }

    @Override
    public String getName() {
        return "西门子S7序列";
    }

    @Override
    public String getDesc() {
        return getName();
    }

    /**
     * 西门子plc的协议都是客户端主动协议
     * @param message
     * @param type
     * @return
     */
    @Override
    protected ServerInitiativeProtocol doGetProtocol(SiemensS7Message message, ProtocolType type) {
        return null;
    }
}
