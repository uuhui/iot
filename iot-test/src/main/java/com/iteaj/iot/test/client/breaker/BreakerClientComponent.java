package com.iteaj.iot.test.client.breaker;

import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.client.component.LengthFieldBasedFrameClientComponent;
import com.iteaj.iot.client.protocol.ServerInitiativeProtocol;
import com.iteaj.iot.test.IotTestProperties;

import java.nio.ByteOrder;

/**
 * 断路器设备模拟组件
 * 使用长度字段解码器解码
 */
public class BreakerClientComponent extends LengthFieldBasedFrameClientComponent<BreakerClientMessage> {

    public BreakerClientComponent(IotTestProperties.BreakerConnectConfig config) {
        super(config, ByteOrder.LITTLE_ENDIAN, 256
                , 0, 4,0, 0, true);
    }

    @Override
    public String getName() {
        return "断路器设备模拟";
    }

    @Override
    public String getDesc() {
        return "用于模拟设备控制, Redis、Taos等数据采集存储测试";
    }

    @Override
    protected ServerInitiativeProtocol doGetProtocol(BreakerClientMessage message, ProtocolType type) {
        return null;
    }

}
